package com.gmail.rusta18.myfiledrop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFileDropApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyFileDropApplication.class, args);
    }
}
