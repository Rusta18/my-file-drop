package com.gmail.rusta18.myfiledrop.controller;

import com.gmail.rusta18.myfiledrop.model.FileSaver;
import com.gmail.rusta18.myfiledrop.model.FilenameFileSaver;
import com.vaadin.ui.Upload.Receiver;

import java.io.OutputStream;

public class FileReceiver implements Receiver {

    private static FileSaver saver;

    @Override
    public OutputStream receiveUpload(String fileName, String mimeType) {
        saver = new FilenameFileSaver();
        return saver.getStream(fileName);
    }
}
