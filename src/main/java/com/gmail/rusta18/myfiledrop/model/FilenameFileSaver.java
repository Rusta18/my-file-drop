package com.gmail.rusta18.myfiledrop.model;

import com.vaadin.ui.Notification;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Saves file in the folder with the name of file (without extension).
 * If folder already exists - creates a new folder with a number at the end.
 */

public class FilenameFileSaver implements FileSaver {


    public FilenameFileSaver(){
    }

    /**
     * @return folder name, which is the same as filename,
     * but without extension and can have extra characters at the end if folder already exists.
     */
    private String getFolderFromFileName(String fileName){
        String shortName;   //name without the extension
        if (fileName.indexOf(".") > 0) {
            shortName = fileName.substring(0, fileName.lastIndexOf("."));
        } else {
            shortName = fileName;
        }
        return getAvailableFolderName(shortName);
    }

    /**
     * Looks for a name that does not exists already.
     * For example, if we have our preferableName is "song" and we already have folder "song" - it will return "song-1".
     * If we already have "song-1" it will return "song-2".
     * @param preferableName name we would like to have for our folder.
     * @return preferableName or (if name already exists) preferableName with an integer at the end
     */
    private String getAvailableFolderName(String preferableName) {
        return getAvailableFolderName(new StringBuilder(preferableName), 0).toString();
    }

    private StringBuilder getAvailableFolderName(StringBuilder preferableName, int attempt){
        StringBuilder tempName = new StringBuilder(preferableName);
        if(attempt>0){
            tempName.append("-");
            tempName.append(attempt);
        }
        if(new File(tempName.toString()).exists()){
            return getAvailableFolderName(preferableName, attempt+1);
        } else {
            return tempName;
        }
    }

    @Override
    public OutputStream getStream(String fileName) {

        FileOutputStream fos = null;
        String folderName = getFolderFromFileName(fileName);
        //creates a new directory
        boolean folderCreated = new File(folderName).mkdir();

        //if folder not created
        if (!folderCreated) {
            Notification.show("Error","Could not create a folder.",
                    Notification.Type.WARNING_MESSAGE);
        }

        //creates FileOutputStream to save file with
        File file = new File(folderName + "//" + fileName);
        try {
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            Notification.show("Error","Could not create FileOutputStream to save a file.",
                    Notification.Type.WARNING_MESSAGE);
        }
        return fos;
    }
}
