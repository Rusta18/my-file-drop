package com.gmail.rusta18.myfiledrop.model;

import java.io.OutputStream;

public interface FileSaver {
    OutputStream getStream(String fileName);   //output string to write to in FileReceiver
}
