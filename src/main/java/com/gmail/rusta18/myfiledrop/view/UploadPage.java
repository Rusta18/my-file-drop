package com.gmail.rusta18.myfiledrop.view;

import com.gmail.rusta18.myfiledrop.controller.FileReceiver;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

@SpringUI(path = "/")
public class UploadPage extends UI {

    private VerticalLayout root;    //contains all the other layouts

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        //setting up root layout
        root = new VerticalLayout();
        root.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setContent(root);

        Label headerText = new Label("Drop your files here.");
        headerText.addStyleName(ValoTheme.LABEL_H1);
        root.addComponent(headerText);

        //adding an upload button with a receiver
        Upload upload = new Upload("", new FileReceiver());
        root.addComponent(upload);

        //finish upload listeners
        upload.addFinishedListener(event -> Notification.show("Fine",
                "File successfully uploaded.",
                Notification.Type.HUMANIZED_MESSAGE));
        upload.addFailedListener(event -> Notification.show("Error",
                "File upload failed.",
                Notification.Type.WARNING_MESSAGE));
    }
}
